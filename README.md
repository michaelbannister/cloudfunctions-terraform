# cloudfunctions-terraform

In which I attempt to document my investigation into why deploying a cloud function through Terraform fails when I 
select the python38 runtime, but works fine with python37.

## Context

I set up Terraform to deploy a Google Cloud Function using the python37 runtime.

All was well until I discovered that [python37 has an ongoing bug](https://issuetracker.google.com/issues/155215191) where 
it doesn't output any useful information at all when a function execution crashes.

I tried to change it to python38 and found my functions failed to deploy.

This repo/readme started off as an attempt to create a "minimal test case" to report a bug to Google, but turned 
into a bit of a chronicle of my debugging process which I thought be interesting to share.
 
## To reproduce the problem

### Requirements

* Terraform >= 0.12.26, < 0.13
* Google credentials exposed to Terraform in one of the [standard ways](https://www.terraform.io/docs/providers/google/guides/provider_reference.html#credentials-1)

### Run

Before running, export variables:

* `GOOGLE_PROJECT`: the GCP project ID where you want to create these resources
* `TF_VAR_staging_bucket`: the name of a GCS bucket (non-existent) which this will create.  If you have an existing bucket you want to use, delete the file `variables.tf`, delete the `resource "google_storage_bucket" "hello_staging"` in `main.tf` and modify the `bucket` parameter of `resource "google_storage_bucket_object" "hello_zip"` to reference your existing bucket.  

Then standard Terraform procedure:
```bash
terraform init
terraform plan
terraform apply
```

This fails!
```
google_cloudfunctions_function.hello: Creating...
google_cloudfunctions_function.hello: Still creating... [10s elapsed]
google_cloudfunctions_function.hello: Still creating... [20s elapsed]
google_cloudfunctions_function.hello: Still creating... [30s elapsed]
google_cloudfunctions_function.hello: Still creating... [40s elapsed]
2020/08/09 12:14:33 [ERROR] <root>: eval: *terraform.EvalApplyPost, err: Error waiting for Creating CloudFunctions Function: Error code 3, message: Build failed: Listing '.'...; Error ID: 0cfbe9cc
2020/08/09 12:14:33 [ERROR] <root>: eval: *terraform.EvalSequence, err: Error waiting for Creating CloudFunctions Function: Error code 3, message: Build failed: Listing '.'...; Error ID: 0cfbe9cc

Error: Error waiting for Creating CloudFunctions Function: Error code 3, message: Build failed: Listing '.'...; Error ID: 0cfbe9cc

  on main.tf line 47, in resource "google_cloudfunctions_function" "hello":
  47: resource "google_cloudfunctions_function" "hello" {
```

### Equivalent GCloud

This assume you've already attempted a Terraform run, so you have a zip file in the GCS bucket (modify the `project` and 
`source` in the script below to work for your project).

```bash
gcloud beta functions deploy hello \
  --project=cloudfunctions-terraform \
  --runtime=python38 \
  --service-account=hello-function@cloudfunctions-terraform.iam.gserviceaccount.com \
  --entry-point=handle \
  --trigger-topic=hello-trigger \
  --source=gs://cloudfunctions-terraform-staging/hello_91948d92dbabc8aa1614b1516bf03a0f.zip \
  --max-instances=1 \
  --memory=128MB \
  --region=europe-west2
```

This fails, too!

```
Deploying function (may take a while - up to 2 minutes)...⠹
For Cloud Build Stackdriver Logs, visit: https://console.cloud.google.com/logs/viewer?project=cloudfunctions-terraform&advancedFilter=resource.type%3Dbuild%0Aresource.labels.build_id%3D79124a26-6733-480f-aa04-d57154c26faf%0AlogName%3Dprojects%2Fcloudfunctions-terraform%2Flogs%2Fcloudbuild
Deploying function (may take a while - up to 2 minutes)...failed.
ERROR: (gcloud.beta.functions.deploy) OperationError: code=3, message=Build failed: Listing '.'...; Error ID: 0cfbe9cc
```
But at least we get a link to the build log now: see [failed_build.log](failed_build.log)

### Passing local source files directly

Note: the `--source` is the only difference to the previous command.

```bash
gcloud beta functions deploy hello \
  --project=cloudfunctions-terraform \
  --runtime=python38 \
  --service-account=hello-function@cloudfunctions-terraform.iam.gserviceaccount.com \
  --entry-point=handle \
  --trigger-topic=hello-trigger \
  --source=hello \
  --max-instances=1 \
  --memory=128MB \
  --region=europe-west2
```

Successful - see [successful_build.log](successful_build.log)

## Analysis

Comparing the two logs I find that both run `python3 -m compileall .` but in the failing build it throws an error:

```
Running "python3 -m compileall ."
Traceback (most recent call last):
  File "/opt/python3.8/lib/python3.8/runpy.py", line 194, in _run_module_as_main
    return _run_code(code, main_globals, None,
  File "/opt/python3.8/lib/python3.8/runpy.py", line 87, in _run_code
    exec(code, run_globals)
  File "/opt/python3.8/lib/python3.8/compileall.py", line 332, in <module>
    exit_status = int(not main())
  File "/opt/python3.8/lib/python3.8/compileall.py", line 314, in main
    if not compile_dir(dest, maxlevels, args.ddir,
  File "/opt/python3.8/lib/python3.8/compileall.py", line 96, in compile_dir
    if not compile_file(file, dfile, force, rx, quiet,
  File "/opt/python3.8/lib/python3.8/compileall.py", line 151, in compile_file
    expect = struct.pack('<4sll', importlib.util.MAGIC_NUMBER,
struct.error: 'l' format requires -2147483648 <= number <= 2147483647
Listing '.'...
Done "python3 -m compileall ." (88.129337ms)
Failure: (ID: 0cfbe9cc) Listing '.'...
```

### Compare the zip files

I can see in the build logs the GCS object that's fetched for each of the builds.

I've downloaded these and saved as `function-source.from-terraform.zip` (which failed) and 
`function-source.from-filesystem.zip` (which worked).

Unzipping and inspecting the contents: they look the same, except that the Terraform-generated zip has set the 
modification date to a fixed date in the future.

```bash
$ ls -la function-source.from-terraform
total 8
drwxr-xr-x  4 michael staff 128 Aug  9 12:50 .
drwxr-xr-x 24 michael staff 768 Aug  9 13:04 ..
-rw-r--r--  1 michael staff 259 Jan  1  2049 main.py
-rw-r--r--  1 michael staff 953 Jan  1  2049 requirements.txt

$ ls -la function-source.from-filesystem
total 8
drwxr-xr-x  4 michael staff 128 Aug  9 12:50 .
drwxr-xr-x 24 michael staff 768 Aug  9 13:04 ..
-rw-r--r--  1 michael staff 259 Aug  9 11:55 main.py
-rw-r--r--  1 michael staff 953 Aug  9 12:02 requirements.txt
```

A bit of googling yields that there's a utility `zipinfo` (also available as `unzip -Z`)

```bash
$ zipinfo function-source.from-filesystem.zip
Archive:  function-source.from-filesystem.zip
Zip file size: 703 bytes, number of entries: 2
-rw-r--r--  2.0 unx      953 b- defN 20-Aug-09 12:02 requirements.txt
-rw-r--r--  2.0 unx      259 b- defN 20-Aug-09 11:55 main.py
2 files, 1212 bytes uncompressed, 483 bytes compressed:  60.1%

$ zipinfo function-source.from-terraform.zip
Archive:  function-source.from-terraform.zip
Zip file size: 742 bytes, number of entries: 2
-rw-r--r--  2.0 unx      259 bl defN 49-Jan-01 00:00 main.py
-rw-r--r--  2.0 unx      953 bl defN 49-Jan-01 00:00 requirements.txt
2 files, 1212 bytes uncompressed, 490 bytes compressed:  59.6%
```

OK, so the files are in a different order, but that seems unlikely to be the problem.

The Terraform one is marked with `bl` and the filesystem one has `b-` in the same place. Could that `l` be a reference 
to the `'l' format` mentioned in the error message?  \[Note from the future: no, it isn't.]

Running zipinfo with verbose (`-v`) option tells me slightly more. Looking at the entry for `main.py`:

```bash
$ zipinfo -v function-source.from-terraform.zip main.py
Archive:  function-source.from-terraform.zip
There is no zipfile comment.

End-of-central-directory record:
-------------------------------

  Zip archive file size:                       742 (00000000000002E6h)
  Actual end-cent-dir record offset:           720 (00000000000002D0h)
  Expected end-cent-dir record offset:         720 (00000000000002D0h)
  (based on the length of the central directory and its expected offset)

  This zipfile constitutes the sole disk of a single-part archive; its
  central directory contains 2 entries.
  The central directory is 115 (0000000000000073h) bytes long,
  and its (expected) offset in bytes from the beginning of the zipfile
  is 605 (000000000000025Dh).


Central directory entry #1:
---------------------------

  main.py

  offset of local header from start of archive:   0
                                                  (0000000000000000h) bytes
  file system or operating system of origin:      Unix
  version of encoding software:                   2.0
  minimum file system compatibility required:     MS-DOS, OS/2 or NT FAT
  minimum software version required to extract:   2.0
  compression method:                             deflated
  compression sub-type (deflation):               normal
  file security status:                           not encrypted
  extended local header:                          yes
  file last modified on (DOS date/time):          2049 Jan 1 00:00:00
  32-bit CRC value (hex):                         427c9b78
  compressed size:                                168 bytes
  uncompressed size:                              259 bytes
  length of filename:                             7 characters
  length of extra field:                          0 bytes
  length of file comment:                         0 characters
  disk number on which file begins:               disk 1
  apparent file type:                             binary
  Unix file attributes (100644 octal):            -rw-r--r--
  MS-DOS file attributes (00 hex):                none

  There is no file comment.

$ zipinfo -v function-source.from-filesystem.zip main.py
Archive:  function-source.from-filesystem.zip
There is no zipfile comment.

End-of-central-directory record:
-------------------------------

  Zip archive file size:                       703 (00000000000002BFh)
  Actual end-cent-dir record offset:           681 (00000000000002A9h)
  Expected end-cent-dir record offset:         681 (00000000000002A9h)
  (based on the length of the central directory and its expected offset)

  This zipfile constitutes the sole disk of a single-part archive; its
  central directory contains 2 entries.
  The central directory is 115 (0000000000000073h) bytes long,
  and its (expected) offset in bytes from the beginning of the zipfile
  is 566 (0000000000000236h).


Central directory entry #2:
---------------------------

  main.py

  offset of local header from start of archive:   364
                                                  (000000000000016Ch) bytes
  file system or operating system of origin:      Unix
  version of encoding software:                   2.0
  minimum file system compatibility required:     MS-DOS, OS/2 or NT FAT
  minimum software version required to extract:   2.0
  compression method:                             deflated
  compression sub-type (deflation):               normal
  file security status:                           not encrypted
  extended local header:                          no
  file last modified on (DOS date/time):          2020 Aug 9 11:55:00
  32-bit CRC value (hex):                         427c9b78
  compressed size:                                165 bytes
  uncompressed size:                              259 bytes
  length of filename:                             7 characters
  length of extra field:                          0 bytes
  length of file comment:                         0 characters
  disk number on which file begins:               disk 1
  apparent file type:                             binary
  Unix file attributes (100644 octal):            -rw-r--r--
  MS-DOS file attributes (00 hex):                none

  There is no file comment.
```

The Terraform-generated zip file has `extended local header: yes` while the gcloud-generated zip file has `no`. That, 
and the last-modified time, are the only apparent differences.

### Check the source of the exception

Now I'll try to find the line of Python where the exception was thrown. A reminder of the exception: 

```
  File "/opt/python3.8/lib/python3.8/compileall.py", line 151, in compile_file
    expect = struct.pack('<4sll', importlib.util.MAGIC_NUMBER,
struct.error: 'l' format requires -2147483648 <= number <= 2147483647
```

I'll assume it's CPython so here's the offending line in [Lib/compileall.py](https://github.com/python/cpython/blob/105cfb5b182da63e8481fcb009e92546d240c6b5/Lib/compileall.py#L151) (on the 3.8 branch)

```python
mtime = int(os.stat(fullname).st_mtime)
expect = struct.pack('<4sll', importlib.util.MAGIC_NUMBER,
                     0, mtime)
with open(cfile, 'rb') as chandle:
    actual = chandle.read(12)
if expect == actual:
    return success
```

Looks like `st_mtime` is getting the last modification time of the Python source file (see Python docs for 
[stat](https://docs.python.org/3.8/library/stat.html#stat.ST_MTIME).

So, I reckon the weird future fixed date that Terraform is using when constructing the zip file must be a value that 
the Python library doesn't like for some reason.

A bit more digging and I realise that 2147483647, the maximum value of a 'long' which the `struct.pack` function accepts, 
is 2038-01-19T03:14:07 when converted from [Epoch time](https://en.wikipedia.org/wiki/Unix_time)

I've even found [an issue](https://bugs.python.org/issue34990) in Python's bug tracker for this. 
It's Python's [Y2K problem](https://en.wikipedia.org/wiki/Year_2000_problem)! (or [one of them](https://bugs.python.org/issue?%40columns=id%2Cactivity%2Ctitle%2Ccreator%2Cassignee%2Cstatus%2Ctype&%40sort=-activity&%40filter=status&%40action=searchid&ignore=file%3Acontent&%40search_text=%22year+2038%22&submit=search&status=-1%2C1%2C2%2C3))

### What next?

So, I'm going to raise an issue with Google, especially since they still haven't made any progress on letting the Terraform provider 
deploy a function from local sources ([hashicorp/terraform-provider-google#3085](https://github.com/hashicorp/terraform-provider-google/issues/3085)), 
and I would prefer to keep this all in Terraform than doing it with `gcloud functions deploy`.

Meanwhile, I can probably script the zipfile creation locally. But I still would like to know why it works OK for the python37 runtime.

Update: the logs for the python37 build look completely different; presumably it simply doesn't attempt to precompile the `.py` files.
