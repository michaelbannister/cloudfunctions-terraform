provider "google" {
  version = "~> 3.28.0"
}
provider "google-beta" {
  version = "~> 3.28.0"
}

provider "archive" {
  version = "~> 1.3"
  // https://registry.terraform.io/providers/hashicorp/archive/latest/docs
}