terraform {
  required_version = "~> 0.12.26"
}

resource "google_storage_bucket" "hello_staging" {
  name = var.staging_bucket
  bucket_policy_only = true
  location = "EU"
}

// generates a zip locally during terraform 'plan'
data "archive_file" "hello_zip" {
  type        = "zip"
  source_dir  = "./hello"
  output_path = "./staging/hello.zip"
  excludes = [
    ".DS_Store",
  ]
}

resource "google_project_service" "pubsub" {
  service = "pubsub.googleapis.com"
  disable_on_destroy = false
}
resource "google_project_service" "cloudfunctions" {
  service = "cloudfunctions.googleapis.com"
  disable_on_destroy = false
}
resource "google_project_service" "cloudbuild" {
  service = "cloudbuild.googleapis.com"
  disable_on_destroy = false
}

// copies the local zip into a bucket, will refresh whenever the local zip changes due to md5 if name
resource "google_storage_bucket_object" "hello_zip" {
  name   = format("hello_%s.zip", data.archive_file.hello_zip.output_md5)
  bucket = google_storage_bucket.hello_staging.name
  source = data.archive_file.hello_zip.output_path
  content_type = "application/zip"
}

resource "google_pubsub_topic" "hello_trigger" {
  name = "hello-trigger"
  depends_on = [google_project_service.pubsub]
}

resource "google_cloudfunctions_function" "hello" {
  provider = google-beta
  name = "hello"
  runtime = "python38"
  service_account_email = google_service_account.function_identity.email

  entry_point = "handle"
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource = google_pubsub_topic.hello_trigger.id
    failure_policy {
      retry = false
    }
  }
  environment_variables = {}
  labels = {}
  source_archive_bucket = google_storage_bucket_object.hello_zip.bucket
  source_archive_object = google_storage_bucket_object.hello_zip.name
  max_instances = 1
  available_memory_mb = 128
  region = "europe-west2"
  depends_on = [google_project_service.cloudfunctions, google_project_service.cloudbuild]
}

resource "google_service_account" "function_identity" {
  account_id = "hello-function"
}
