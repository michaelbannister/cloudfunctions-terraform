variable "staging_bucket" {
  description = "Name of the GCS bucket to be created for staging the function source zip for deployment."
}