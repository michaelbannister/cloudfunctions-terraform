import base64
import json


def handle(event, context):
    name = "World"
    if 'data' in event:
        data = json.loads(base64.b64decode(event['data']).decode('utf-8'))
        if 'name' in data:
            name = data['name']

    print(f"Hi, {name}")
